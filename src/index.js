import React from 'react';
import RootNavigation from './stacks';

const AppRoot = () => {
  return (
    <RootNavigation />
  );
}

export default AppRoot;