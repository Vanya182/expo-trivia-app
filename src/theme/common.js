import { StyleSheet } from 'react-native';

const Common = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingLeft: 20,
    paddingRight: 20
  },
  safeArea: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  content: {
    backgroundColor: '#fff',
    paddingLeft: 20,
    paddingRight: 20
  }
});

export default Common;