import { StyleSheet } from 'react-native';

const Helpers = StyleSheet.create({
  fullHeight: {
    flex: 1,
  },
  fullWidth: {
    width: '100%'
  },
  flexCenter: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  flexDirectionRow: {
    flexDirection: 'row'
  },
  alignItemsCenter: {
    alignItems: 'center'
  },
  alignSelfCenter: {
    alignSelf: 'center'
  },
  justifyContentCenter: {
    justifyContent: 'center'
  },
  alignItemsEnd: {
    alignItems: 'flex-end'
  },
  textAlignCenter: {
    textAlign: 'center'
  },
  positionRelative: {
    position: 'relative'
  }
});

export default Helpers;