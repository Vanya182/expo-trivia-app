import React, { useState, useEffect } from 'react';

const FETCH_URL = 'https://opentdb.com/api.php?amount=10&difficulty=medium&type=boolean';
const FETCH_CONFIG = {
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

const fetchQuizQuestions = async () => {
  try {
    const response = await fetch(FETCH_URL, FETCH_CONFIG);
    const { results } = await response.json();
    const resultsTransformed = results.map(result => {
      return {...result, correct_answer: (result.correct_answer === 'True') ? true : false}
    });
    return resultsTransformed;
  } catch {
    return null;
  }
};

export { fetchQuizQuestions};
