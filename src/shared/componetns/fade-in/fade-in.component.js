import React, { useState, useEffect } from 'react';
import { Animated, Text, View } from 'react-native';

const FadeInView = (props) => {
  const [fade] = useState(new Animated.Value(0));
  const [translateY] = useState(new Animated.Value(20))
  const { start } = props;
  
  React.useEffect(() => {
    if (start) {
      Animated.timing(
        fade,
        {
          toValue: 1,
          duration: 500,
        }
      ).start();
      Animated.timing(
        translateY,
        {
          toValue: 0,
          duration: 300,
        }
      ).start(); 
    }
  }, [start])

  return (
    <Animated.View                 
      style={{
        ...props.style,
        opacity: fade,
        transform: [{ translateY }]
      }}
    >
      {props.children}
    </Animated.View>
  );
}

export default FadeInView;