import { combineReducers } from 'redux';
import quizReducer from './quiz';

const rootReducer = combineReducers({
  quizStore: quizReducer
});

export default rootReducer;
