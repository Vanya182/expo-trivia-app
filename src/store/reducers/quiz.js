import { SET_QUESTIONS, INC_CORRECT_ANSWERS, INC_INDEX } from '../types';

const initialState = {
  currentIndex: 0,
  correctAnswers: 0,
  questions: []
}

const quizReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_QUESTIONS: {
      return {
        ...state,
        questions: action.payload,
        correctAnswers: 0,
        currentIndex: 0
      }
    }
    case INC_CORRECT_ANSWERS: {
      return {
        ...state,
        correctAnswers: state.correctAnswers + 1
      }
    }
    case INC_INDEX: {
      return {
        ...state,
        currentIndex: state.currentIndex + 1
      }
    }
    default:
      return state;
  }
}

export default quizReducer; 