import { SET_QUESTIONS, INC_CORRECT_ANSWERS, INC_INDEX } from '../types';

const QuizActions = {
  setQuestions: (value) => {
    return {
      type: SET_QUESTIONS,
      payload: value
    }
  },
  incrementCorrectAnswers: () => {
    return {
      type: INC_CORRECT_ANSWERS
    }
  },
  incrementIndex: () => {
    return {
      type: INC_INDEX
    }
  }
}

export default QuizActions;