import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import LoadingStack from './loading/loading.stack';
import QuizStack from './quiz/quiz.stack';
import LoadingScreen from './loading/screens/loading/loading.screen';

const RootNavigation = createSwitchNavigator(
  {
    Loading: LoadingScreen,
    Quiz: QuizStack,
  },
  {
    initialRouteName: 'Loading',
  }
);


export default createAppContainer(RootNavigation);
