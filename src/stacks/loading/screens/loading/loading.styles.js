import { StyleSheet } from 'react-native';

const LoadingStyles = StyleSheet.create({
  title: {
    paddingBottom: 50
  },
  subtitle: {
    paddingTop: 20
  },
  lottie: {
    width: '60%',
  },
  startButton: {
    marginTop: 30
  },
  isHidden: {
    opacity: 0
  },
  isVisible: {
    opacity: 1
  }
});

export default LoadingStyles;