import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { View, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';
import { human } from 'react-native-typography';
import { fetchQuizQuestions } from '../../../../services';
import { QuizActions } from '../../../../store/actions';
import { Common, Helpers } from '../../../../theme';
import { Container, Button, Text } from 'native-base';
import { FadeIn } from '../../../../shared';
import LottieView from "lottie-react-native";
import LoadingStyles from './loading.styles';

const lottieAnimation = require('../../../../assets/6308-risinghand.json');

const LoadingScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [dataLoaded, setDataLoaded] = useState(false);
  const lottieContainerRef = useRef(null);
  const getData = async () => {
    const results = await fetchQuizQuestions();
    setTimeout(() => {
      dispatch(QuizActions.setQuestions(results));
      setDataLoaded(true);
    }, 500);
  };
  
  useEffect(() => {
    getData();
    lottieContainerRef.current.play();  
  }, []);
  
  const onStartQuiz = () => {
    navigation.navigate('Quiz');
  };
  
  return (
    <Container style={Common.container}>
      <SafeAreaView style={[Common.content, Helpers.fullHeight, Helpers.flexCenter]}>
        <View>
          <Text style={[human.largeTitle, Helpers.textAlignCenter, LoadingStyles.title]}>Trivia Quiz</Text>
          <LottieView
            ref={lottieContainerRef}
            style={LoadingStyles.lottie}
            source={lottieAnimation}
          />
          <Text style={[human.subhead, Helpers.textAlignCenter, LoadingStyles.subtitle]}>
            Hang on. Prepearing questions...
          </Text>
        </View>
        <View>
          <FadeIn start={dataLoaded}>
            <Button primary style={LoadingStyles.startButton} onPress={onStartQuiz}>
              <Text>Start Quiz</Text>
            </Button>
          </FadeIn> 
        </View>
      </SafeAreaView>
    </Container>
  );  
};

LoadingScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired
}

export default LoadingScreen;