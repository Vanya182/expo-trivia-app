import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import LottieView from "lottie-react-native";

const round0 = require('../../../../assets/round0.json');
const round1 = require('../../../../assets/round1.json');
const round2 = require('../../../../assets/round2.json');
const round3 = require('../../../../assets/round3.json');
const round4 = require('../../../../assets/round4.json');
const round5 = require('../../../../assets/round5.json');
const round6 = require('../../../../assets/round6.json');
const round7 = require('../../../../assets/round7.json');
const round8 = require('../../../../assets/round8.json');
const round9 = require('../../../../assets/round9.json');
const animationAliases = {
  0: round0,
  1: round1,
  2: round2,
  3: round3,
  4: round4,
  5: round5,
  6: round6,
  7: round7,
  8: round8,
  9: round9
}
const RoundNumber = ({ index }) => {
  const lottieContainerRef = useRef(null);
  
  useEffect(() => {
    if (lottieContainerRef && lottieContainerRef.current) {
      lottieContainerRef.current.play();  
    }
  }, [lottieContainerRef]);
  
  return  (animationAliases[index] ? <LottieView
    loop={false}
    ref={lottieContainerRef}
    style={{width: 100}}
    source={animationAliases[index]}
  /> : null);
};

RoundNumber.propTypes = {
  index: PropTypes.number.isRequired
}

RoundNumber.defaultProps = {
  index: 1
};

export default RoundNumber;