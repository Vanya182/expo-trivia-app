import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchQuizQuestions } from '../../../../services';
import { QuizActions } from '../../../../store/actions';
import { View, SafeAreaView } from 'react-native';
import { Container, Text, Button } from 'native-base';
import { human } from 'react-native-typography';
import { Common, Helpers } from '../../../../theme';
import ResultsStyles from './results.styles';

const ResultsScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const { time } = navigation.state.params;
  const { correctAnswers } = useSelector(state => state.quizStore);

  const onNewQuiz = async () => {
      const results = await fetchQuizQuestions();
      dispatch(QuizActions.setQuestions(results));
      navigation.navigate('Quiz');
  };
  
  return (
    <Container style={Common.container}>
      <SafeAreaView style={[Common.content, Helpers.fullHeight, Helpers.flexCenter]}>
        <View>
          <Text style={[human.largeTitle, Helpers.textAlignCenter, ResultsStyles.title]}>Results</Text>
          <Text style={[human.headline, Helpers.textAlignCenter]}>
            Correct answers: {correctAnswers}
          </Text>
          { time && <Text style={[human.headline, Helpers.textAlignCenter, ResultsStyles.subtitle]}>
            Time: {(time/1000).toFixed(0)}
          </Text>}
          <Button primary onPress={onNewQuiz} style={[Helpers.justifyContentCenter, Helpers.textAlignCenter]}>
            <Text>Play again</Text>
          </Button>
        </View>
      </SafeAreaView>
    </Container>
  )
};

ResultsScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired
};

export default ResultsScreen;