import { StyleSheet } from 'react-native';

const ResultsStyles = StyleSheet.create({
  title: {
    paddingBottom: 5 
  },
  subtitle: {
    paddingBottom: 50
  }
});

export default ResultsStyles;