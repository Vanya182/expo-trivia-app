import { StyleSheet } from 'react-native';

const QuizStyles = StyleSheet.create({
  questionInfoBox: {
    width: '100%'
  },
  roundNumber: {
    
  },
  category: {
    paddingTop: 20,
    paddingBottom: 20
  },
  question: {
    paddingBottom: 50
  },
  itemsRow: {
    alignSelf: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems:'flex-start',
  },
  buttonBox: {
    marginLeft: 25,
    marginRight: 25
  },
  stopwatchBox: {
    position: 'absolute', 
    bottom: 80
  }
});

export default QuizStyles;