import React, { useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { View, SafeAreaView } from 'react-native';
import { human } from 'react-native-typography';
import { Stopwatch } from 'react-native-stopwatch-timer'
import { QuizActions } from '../../../../store/actions';
import { Root, Container, Text, Button } from 'native-base';
import { Common, Helpers } from '../../../../theme';
import { FadeIn } from '../../../../shared';
import RoundNumber from '../../components/round-number/round-number.component';
import QuizStyles from './quiz.styles';

const QuizScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [timeCount, setTimeCount] = useState(null);
  const [stopwatchActive, setStopwatchActive] = useState(false);
  const [isMounted, setIsMounted] = useState(false);
  const { questions, currentIndex } = useSelector(state => state.quizStore);
  
  useEffect(() => {
    setStopwatchActive(true);
    setIsMounted(true);
    return () => {
      setIsMounted(false);
      setStopwatchActive(false);
    }
  }, [questions]);
  
  useEffect(() => {
    if (currentIndex === 10) {
      setIsMounted(false);
      setStopwatchActive(false);
      navigation.navigate('Results', { time: timeCount});  
    }
  }, [currentIndex])
  
  const prepeareQuestionText = (text) => {
    const regex = /(<([^>]+)>)/ig;
    return text.replace(regex, '').replace(/&quot;/g,'"');
  }
  const onAssumption = ((assumption, answer) => {
    if (assumption === answer) {
      dispatch(QuizActions.incrementCorrectAnswers());
    }
    dispatch(QuizActions.incrementIndex());
  });
  
  return (
    <Root>
      <Container style={Common.container}>
        <SafeAreaView style={Common.safeArea}>
          {
            questions ? questions.map(({
              category,
              question,
              correct_answer
            }, index) => {
              if (index === currentIndex) {
                return (
                  <FadeIn start={true} key={index} style={QuizStyles.questionInfoBox}>
                    <View style={[Helpers.alignItemsCenter, Helpers.justifyContentCenter]}>
                        { index === 9 ? 
                          <View style={QuizStyles.itemsRow}>
                            <View style={{marginRight: -30}}>
                              <RoundNumber index={1}/>
                            </View>
                            <RoundNumber index={0} />
                          </View> : <RoundNumber index={index + 1} />
                        }
                        <Text style={[human.title1, QuizStyles.category, Helpers.textAlignCenter]}>
                          {category}
                        </Text>
                        <Text style={[human.body, QuizStyles.question, Helpers.textAlignCenter]}>
                          {prepeareQuestionText(question)}
                        </Text>
                        <View style={QuizStyles.itemsRow}>
                          <View style={QuizStyles.buttonBox}>
                          <Button light onPress={() => {onAssumption(false, correct_answer)}}>
                                <Text>False</Text>
                              </Button>
                          </View>
                              <View style={QuizStyles.buttonBox}>
                                <Button primary onPress={() => {onAssumption(true, correct_answer)}}>
                                <Text>True</Text>
                              </Button>
                              </View>
                        </View>
                    </View>
                  </FadeIn>
                );
              }
            }) : null
          }
          <View style={QuizStyles.stopwatchBox}>
            <FadeIn start={true}>
              {
                isMounted ? <Stopwatch start={stopwatchActive} getMsecs={value => setTimeCount(value)}/> : null
              }
            </FadeIn>
          </View>
        </SafeAreaView>
      </Container>
    </Root>
  )
};

QuizScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired  
};

export default QuizScreen;