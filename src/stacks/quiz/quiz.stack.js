import { createStackNavigator } from 'react-navigation-stack';
import QuizScreen from './screens/quiz/quiz.screen';
import ResultsScreen from './screens/results/results.screen';

const QuizStack = createStackNavigator({
  Quiz: {
    screen: QuizScreen,
    navigationOptions: () => ({
      header: null
    })
  },
  Results: {
    screen: ResultsScreen,
    navigationOptions: () => ({
      header: null
    })
  }
});

export default QuizStack;
