/* eslint-disable global-require */
import { Asset } from 'expo-asset';

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    }
    return Asset.fromModule(image).downloadAsync();
  });
}

export default async () => Promise.all([
  cacheImages([
    // // Common
    require('./assets/round0.json'),
    require('./assets/round1.json'),
    require('./assets/round2.json'),
    require('./assets/round3.json'),
    require('./assets/round4.json'),
    require('./assets/round5.json'),
    require('./assets/round6.json'),
    require('./assets/round7.json'),
    require('./assets/round8.json'),
    require('./assets/round9.json'),
  ]),
])
