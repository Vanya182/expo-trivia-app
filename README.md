Trivia app.
To setup local environment please check that you have node installed on your machine.

1. Install expo-cli package. By running "npm install -g expo-cli"
2. Clone repo
3. Run "npm install"
4. Run "expo start" or "npm run start "
5. Check terminal output you should be ready to go from this step.